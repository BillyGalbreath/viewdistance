package net.pl3x.bukkit.viewdistance;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class ViewDistance extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        try {
            Class.forName("com.destroystokyo.paper.PaperConfig");
        } catch (ClassNotFoundException e) {
            error("&4#&4#############################################################################&4#");
            error("&4#&c                 This plugin only works on Paper servers!                    &4#");
            error("&4#&4                                                                             &4#");
            error("&4#&4     To prevent server crashes and other undesired behavior this plugin      &4#");
            error("&4#&4       is disabling itself from running. Please remove the plugin jar        &4#");
            error("&4#&4             from your plugins directory to free up some memory.             &4#");
            error("&4#&4#############################################################################&4#");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        Bukkit.getPluginManager().registerEvents(this, this);
    }

    private void error(String message) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        setViewDistance(event);
    }

    @EventHandler
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        setViewDistance(event);
    }

    @EventHandler
    public void onPlayerChangedChunk(PlayerMoveEvent event) {
        Location to = event.getTo();
        Location from = event.getFrom();
        if (to.getBlockX() == from.getBlockX() && to.getBlockZ() == from.getBlockZ()) {
            return;
        }
        Chunk toChunk = to.getChunk();
        Chunk fromChunk = from.getChunk();
        if (toChunk.getX() == fromChunk.getX() && toChunk.getZ() == fromChunk.getZ()) {
            return;
        }
        setViewDistance(event);
    }

    private void setViewDistance(PlayerEvent event) {
        Player player = event.getPlayer();
        int distance = getViewDistance(player);
        player.setViewDistance(distance);
        if (getConfig().getBoolean("debug-mode", false)) {
            Bukkit.getConsoleSender().sendMessage("[ViewDistance] [DEBUG] View distance for " + player.getName() + " set to " + distance);
        }
    }

    private int getViewDistance(Player player) {
        if (player.isOp()) {
            return Bukkit.getViewDistance();
        }
        int elytraLimit = getConfig().getInt("elytra-limit", 2);
        if (elytraLimit >= 0 && player.isGliding()) {
            return elytraLimit;
        }
        int min = 2;
        int max = 32;
        for (int distance = max; distance >= min; distance--) {
            if (player.hasPermission("view.distance." + distance)) {
                return distance;
            }
        }
        return Bukkit.getViewDistance();
    }
}
